# FileSize - A simple but cool Python disk utility.

## 📌 Intoduction

We are an open source Python project that is currently developing a disk utility tool that allows you to scan any folder on your system(as long you have access!) or just list the total size of all mounted disks.
We are open to any contribution so if you feel like you have a good idea and want to help out, please feel free to contribute!

## 📙 Requirement

- [Python](https://www.python.org/)

## ⚙️ Setup

- Download the latest release [here](https://github.com/icmpp/filesize/releases/tag/latest)
- Extract your downloaded .zip file
- Open a terminal window and navigate to the extracted folder
- Simply run the Python script via your terminal window
- Give a star to our project!

## 🚀 Features

- Open Source
- Runs on any OS that runs Python
- Simple to use CLI
- Scan the entire mounted disk and see it's current size & used space
- Scan particular folders to see their current size & used space
- See all mounted disks and their respective size & used space

## 🗺️ Roadmap

- [ ] Filter by file type
- [ ] Sort by size
- [ ] Scan multiple drives
- [ ] Multiple export options
- [ ] Compression/decompression
- [ ] Duplicate detection
- [ ] Security scan
- [ ] Disk cleanup
- [ ] Graphical user interface (optional)

## 💥 How to Contribute?

- Take a look at the existing [issues](https://github.com/icmpp/filesize/issues) or [create](https://github.com/icmpp/filesize/issues/new) a new one.
- Wait for the issue to be assigned to you after which you can start working on it.
- [Fork](https://github.com/icmpp/filesize/fork) the repo and create a Branch for any Issue that you are working upon.
- Create a [Pull](https://github.com/icmpp/filesize/compare) Request which will be promptly reviewed and suggestions would be added to improve it.
- Having difficulty in contributing? Watch the [Contribution Guide](https://youtu.be/c6b6B9oN4Vg) for a detailed explanation.
