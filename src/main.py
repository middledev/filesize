import os
import ctypes
import psutil
from colorama import init, Fore, Style

ctypes.windll.kernel32.SetConsoleTitleW("FileSize - Disk Utility")

def format_size(size):
    """Format size to be human-readable"""
    power = 2**10
    n = 0
    power_labels = {0: '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    return f"{size:.2f} {power_labels[n]}B"

def get_dir_size(path='.'):
    """Return size of directory in bytes"""
    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_dir_size(entry.path)
    return total

def display_folder_size(path):
    """Display the size of a file or directory"""
    try:
        if os.path.isfile(path):
            print(f"{path}: {format_size(os.path.getsize(path))}")
        elif os.path.isdir(path):
            total_size = 0
            for dirpath, dirnames, filenames in os.walk(path):
                for f in filenames:
                    fp = os.path.join(dirpath, f)
                    try:
                        total_size += os.path.getsize(fp)
                    except PermissionError:
                        continue
            print(Fore.GREEN + Style.BRIGHT + "==== Result ====")
            print(Fore.WHITE + "Path:" + Fore.BLUE + f" {path}")
            print(Fore.WHITE + "Size:" + Fore.BLUE + f" {format_size(total_size)}")
            print(Fore.WHITE + "")
        else:
            print(f"{path}: No such file or directory")
    except PermissionError:
        print(f"{path}: Permission Denied")

def display_disk_usage():
    """Display total size of all disks"""
    total_size = 0
    print(Fore.GREEN + Style.BRIGHT + "==== Disk Usage ====")
    for partition in psutil.disk_partitions():
        if partition.mountpoint != '/':
            try:
                usage = psutil.disk_usage(partition.mountpoint)
                total_size += usage.total
                print(Fore.WHITE + "Disk:" + Fore.BLUE + f" {partition.mountpoint}")
                print(Fore.WHITE + "Size:" + Fore.BLUE + f" {format_size(usage.total)}")
                print(Fore.WHITE + "")
            except PermissionError:
                continue
    print(Fore.BLUE + Style.BRIGHT + "> " + Fore.WHITE + "Total:" + Fore.BLUE + f" {format_size(total_size)}")
    print(Fore.WHITE + "")

def display_disk_partitions():
    """Display all mounted disks and their partitions"""
    print(Fore.WHITE + "")
    init()  # initialize colorama
    print(Fore.GREEN + Style.BRIGHT + "==== Disk Partitions ====")
    for partition in psutil.disk_partitions(): 
        print(Fore.WHITE + "Device:" + Fore.BLUE + f" {partition.device}")
        print(Fore.WHITE + "Mount Point:" + Fore.BLUE + f" {partition.mountpoint}")
        print(Fore.WHITE + "File System Type:" + Fore.BLUE + f" {partition.fstype}")
        try:
            usage = psutil.disk_usage(partition.mountpoint)
            print(Fore.WHITE + "Total Size:" + Fore.BLUE + f" {format_size(usage.total)}")
            print(Fore.WHITE + "Used:" + Fore.BLUE + f" {format_size(usage.used)}")
            print(Fore.WHITE + "Free:" + Fore.BLUE + f" {format_size(usage.free)}")
            print(Fore.WHITE + "Percentage Used:" + Fore.BLUE + f" {usage.percent}%")
        except PermissionError:
            print(Fore.RED + "Error: Could not retrieve usage information")
        print(Fore.WHITE + "")

def scan_drive(path, min_size=10**7):
    """Scan a single mounted drive and export a detailed list of larger folders with their percentage taken up"""
    try:
        total_size = psutil.disk_usage(path).total
        print(f"Scanning {path} ({format_size(total_size)})...")
        folders = {}
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in filenames:
                filepath = os.path.join(dirpath, filename)
                size = os.path.getsize(filepath)
                if size >= min_size:
                    folder_path = os.path.split(filepath)[0]
                    if folder_path not in folders:
                        folders[folder_path] = size
                    else:
                        folders[folder_path] += size
        print(f"Exporting results to {path}_folder_sizes.html...")
        with open(f"{path}_folder_sizes.html", "w") as f:
            f.write("<html>\n")
            f.write("<head>\n")
            f.write("<title>Folder Sizes</title>\n")
            f.write("</head>\n")
            f.write("<body>\n")
            f.write("<table>\n")
            f.write("<tr><th>Folder</th><th>Size</th><th>Percentage</th></tr>\n")
            for folder_path, size in sorted(folders.items(), key=lambda x: x[1], reverse=True):
                percentage = (size / total_size) * 100
                link_path = folder_path.replace(path, "", 1)
                link_path_parts = link_path.split(os.path.sep)
                link_parts = [""]
                for i in range(len(link_path_parts)):
                    part_path = os.path.join(path, *link_parts[:i+1])
                    part_link_path = os.path.join(*link_parts[:i+1])
                    part_link = f"{part_link_path}_folder_sizes.html"
                    if i == 0:
                        link_text = "/"
                    else:
                        link_text = link_path_parts[i-1]
                    if i == len(link_path_parts) - 1:
                        folder_text = link_path_parts[i]
                    else:
                        folder_text = ""
                    link = f'<a href="{part_link}">{link_text}</a>'
                    folder = f'{link}{os.path.sep}{folder_text}'
                    link_parts.append(link_path_parts[i])
                f.write(f"<tr><td>{folder}</td><td>{format_size(size)}</td><td>{percentage:.2f}%</td></tr>\n")
            f.write("</table>\n")
            f.write("</body>\n")
            f.write("</html>\n")
        print("Export complete.")
    except PermissionError:
        print(f"{path}: Permission Denied")

def display_version():
    """Display version of the script"""
    print(Fore.GREEN + Style.BRIGHT + "==== FileSize ====")
    print(Fore.WHITE + "FileSize version 1.0.0")
    print(Fore.WHITE + "")

def display_shortcuts():
    """Display shortcuts"""
    print(Fore.WHITE + "")
    print(Fore.GREEN + Style.BRIGHT + "==== FileSize Shortcuts ====")
    print(Fore.BLUE + "H. Return to the home screen")
    print(Fore.WHITE + "")

def display_menu():
    """Display the home screen"""
    init()  # initialize colorama
    print(Fore.GREEN + Style.BRIGHT + "==== FileSize ====")
    print(Fore.WHITE + "Welcome to FileSize! Please select an option:")
    print(Fore.BLUE + "1. Disk")
    print(Fore.BLUE + "2. Shortcuts")
    print(Fore.BLUE + "3. Version")
    print(Fore.RED + "4. Exit")
    print(Fore.WHITE + "")

def display_disk_submenu():
    """Display the disk submenu"""
    print(Fore.WHITE + "")
    init()  # initialize colorama
    print(Fore.GREEN + Style.BRIGHT + "==== Disk Menu ====")
    print(Fore.WHITE + "Please select an option:")
    print(Fore.BLUE + "1. Display size of a file or directory")
    print(Fore.BLUE + "2. Display total size of all disks")
    print(Fore.BLUE + "3. View mounted disks and their partitions")
    print(Fore.BLUE + "4. Scan a single mounted drive")
    print(Fore.RED + "5. Return to home screen")
    print(Fore.WHITE + "")

def main():
    """Main program loop"""
    display_menu()
    returned_home = False
    while True:
        choice = input("Enter a number or letter to select an option: ")
        if choice == '1':
            while True:
                display_disk_submenu()
                disk_choice = input("Enter a number or letter to select an option: ")
                if disk_choice == '1':
                    path = input("Enter the path to the file or directory: ")
                    display_folder_size(path)
                    input("Press Enter to return")
                elif disk_choice == '2':
                    display_disk_usage()
                    input("Press Enter to return")
                elif disk_choice == '3':
                    display_disk_partitions()
                    input("Press Enter to return")
                elif disk_choice == '4':
                    path = input("Enter the path of the mounted drive to scan: ")
                    scan_drive(path)
                    input("Press Enter to return")
                elif disk_choice.lower() == '5':
                    break
                elif disk_choice.lower() == 'h':
                    display_menu()
                else:
                    print("Invalid choice, please try again.")
            returned_home = True
        elif choice == '2':
            display_shortcuts()
            returned_home = True
        elif choice == '3':
            display_version()
            returned_home = True
        elif choice == '4':
            print("Exiting program...")
            break
        elif choice.lower() == 'h':
            display_menu()
            returned_home = False
            continue
        else:
            print("Invalid choice, please try again.")
            returned_home = True
        if returned_home:
            input("Press Enter to return")
            display_menu()
        else:
            returned_home = True

if __name__ == '__main__':
    main()
